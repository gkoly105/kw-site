const hostAddrPlaceKey = '$2a$10$dyqAWxQNlPmQai0lVhox6.xYWeJqop3WLPzz1aOJVUemevktXsVGW';
export var hostAddr = ''
export const ERRORS = {
    NETWORK: 'Network error',
    TIMEOUT: 'Timeout error',
    REQFAILED: 'Network request failed',
    NOTFOUND: 'Not found',
    METHOD: 'Invalid method',
    FORMAT: 'Invalid format',
    ARGS: 'Invalid args',
    CREDS: 'Invalid credentials',
    AUTH: 'Auth required',
    PERM: 'Permission denied',
}

export function getError(error){
    if (error.message == ERRORS.TIMEOUT ||
        error.message == ERRORS.REQFAILED){
        return ERRORS.NETWORK
    }
    return error.message
}

export function imageUrl(path){
  return hostAddr + '/api' + path;
}


export function checkAuth(callback){
    console.log('check auth')
    // Заглушка
    // callback(true)
    // return
    /////////////////////
  
    fetch(hostAddr + '/api/check_auth', {
      method: 'GET',
      credentials: 'same-origin',
      mode:'no-cors'
    })
    .then(resp => {
      // console.log('check auth resp', resp);
      return resp.json();
    })
    .then(res => {
      // console.log('resp json', res);
      callback(res.data, res.reason);
    })
    .catch(error => {
      // console.log('error', error)
      error = getError(error);
      // console.log('error', error)
      callback(undefined, error);
    })
  }

  export function login(login, password, callback){
    // console.log('login')
    fetch(hostAddr + '/api/login', {
      method: 'POST',
      credentials: 'same-origin',
      body: JSON.stringify({
        login: login,
        password: password,
      })
    })
    .then(resp => {
      // console.log('login', resp);
      return resp.json();
    })
    .then(res => {
      // console.log('resp json', res)
      callback(res.data, res.reason);
    })
    .catch(error => {
      // console.log('error', error)
      error = getError(error);
      callback(undefined, error);
    })
  }
  
  export function getProfile(callback) {
    // console.log('get profile')
    fetch(hostAddr + '/api/profile', {
        method: 'GET',
        credentials: 'same-origin',
      })
      .then(resp => {
        // console.log('get profile resp', resp);
        return resp.json();
      })
      .then(res => {
        // console.log('resp json', res)
        callback(res.data, res.reason);
      })
      .catch(error => {
        // console.log('error', error)
        error = getError(error);
        // console.log('error', error)
        callback(undefined, error);
      })
  }

export function getParentList(callback) {
    // console.log('get parent list')
    fetch(hostAddr + '/api/parent_list', {
        method: 'GET',
        credentials: 'same-origin',
      })
      .then(resp => {
        // console.log('get parent list resp', resp);
        return resp.json();
      })
      .then(res => {
        // console.log('resp json', res)
        callback(res.data, res.reason);
      })
      .catch(error => {
        // console.log('error', error)
        error = getError(error);
        // console.log('error', error)
        callback(undefined, error);
      })
}

export function getChildList(callback) {
    // console.log('get child list')
    fetch(hostAddr + '/api/child_list', {
        method: 'GET',
        credentials: 'same-origin',
      })
      .then(resp => {
        // console.log('get child list resp', resp);
        return resp.json();
      })
      .then(res => {
        // console.log('resp json', res)
        callback(res.data, res.reason);
      })
      .catch(error => {
        // console.log('error', error)
        error = getError(error);
        // console.log('error', error)
        callback(undefined, error);
      })
}

export function addTask(data, callback) {
  // console.log('add task')
    fetch(hostAddr + '/api/add_task', {
      method: 'POST',
      credentials: 'same-origin',
      body: JSON.stringify(data),
    })
    .then(resp => {
      // console.log('add task resp', resp);
      return resp.json();
    })
    .then(res => {
      // console.log('resp json', res)
      callback(res.status == 'ok', res.reason);
    })
    .catch(error => {
      // console.log('error', error)
      error = getError(error);
      // console.log('error', error)
      callback(false, error);
    })
}

export function logout(callback){
  // console.log('logout')
  fetch(hostAddr + '/api/logout', {
    method: 'GET',
    credentials: 'same-origin',
  })
  .then(resp => {
    // console.log('check auth', resp);
    return resp.json();
  })
  .then(res => {
    // console.log('resp json', res)
    if (callback)
      callback(res.status == 'ok');
  })
  .catch(error => {
    // console.log('error', error)
    if (callback)
    {
      error = getError(error);
      callback(false, error);
    }
  })
}

export function doTask(task_id, callback) {
  // console.log('do task')

  let data = {task_id: task_id}

  fetch(hostAddr + '/api/do_task', {
      method: 'POST',
      credentials: 'same-origin',
      body: JSON.stringify(data),
    })
    .then(resp => {
      //console.log('do task res', resp);
      return resp.json();
    })
    .then(res => {
      //console.log('resp json', res)
      callback(res.data, res.reason);
    })
    .catch(error => {
      //console.log('error', error)
      error = getError(error);
      //console.log('error', error)
      callback(undefined, error);
    })
}

export function confirmTask(task_id, callback) {
  // console.log('confirm task')

  let data = {task_id: task_id}

  fetch(hostAddr + '/api/confirm_task', {
      method: 'POST',
      credentials: 'same-origin',
      body: JSON.stringify(data),
    })
    .then(resp => {
      // console.log('confirm task res', resp);
      return resp.json();
    })
    .then(res => {
      // console.log('resp json', res)
      callback(res.data, res.reason);
    })
    .catch(error => {
      // console.log('error', error)
      error = getError(error);
      // console.log('error', error)
      callback(undefined, error);
    })
}

export function getNotifList(callback) {
  // console.log('get notifs list')
  fetch(hostAddr + '/api/notif_list', {
    method: 'GET',
    credentials: 'same-origin',
  })
  .then(resp => {
    // console.log('get notif list resp', resp);
    return resp.json();
  })
  .then(res => {
    // console.log('resp json', res)
    callback(res.data, res.reason);
  })
  .catch(error => {
    // console.log('error', error)
    error = getError(error);
    // console.log('error', error)
    callback(undefined, error);
  })
}

export function remindTask(task_id, callback) {
  //console.log('remind task')

  let data = {task_id}

  fetch(hostAddr + '/api/remind_task', {
    method: 'POST',
    credentials: 'same-origin',
    body: JSON.stringify(data),
  })
  .then(resp => {
    //console.log('remind task resp', resp);
    return resp.json();
  })
  .then(res => {
    //console.log('resp json', res)
    callback(res.status == 'ok', res.reason);
  })
  .catch(error => {
    //console.log('error', error)
    error = getError(error);
    //console.log('error', error)
    callback(false, error);
  })
}

export function editTask(data, callback) {
  // console.log('edit task')
    fetch(hostAddr + '/api/edit_task', {
      method: 'POST',
      credentials: 'same-origin',
      body: JSON.stringify(data),
    })
    .then(resp => {
      // console.log('edit task resp', resp);
      return resp.json();
    })
    .then(res => {
      // console.log('resp json', res)
      callback(res.status == 'ok', res.reason);
    })
    .catch(error => {
      // console.log('error', error)
      error = getError(error);
      // console.log('error', error)
      callback(false, error);
    })
}

export function penaltyChild(child_id, amount, task_id, callback) {
  // console.log('penalty child')

  let data = {child_id, amount}
  if (task_id !== undefined) {
    data.task_id = task_id;
  }

  fetch(hostAddr + '/api/penalty', {
      method: 'POST',
      credentials: 'same-origin',
      body: JSON.stringify(data),
    })
    .then(resp => {
      // console.log('penalty child res', resp);
      return resp.json();
    })
    .then(res => {
      // console.log('resp json', res)
      callback(res.data, res.reason);
    })
    .catch(error => {
      // console.log('error', error)
      error = getError(error);
      // console.log('error', error)
      callback(undefined, error);
    })
}

export function removeTask(task_id, callback) {
  // console.log('remove task')

  let data = {task_id}

    fetch(hostAddr + '/api/remove_task', {
      method: 'POST',
      credentials: 'same-origin',
      body: JSON.stringify(data),
    })
    .then(resp => {
      // console.log('remove task resp', resp);
      return resp.json();
    })
    .then(res => {
      // console.log('resp json', res)
      callback(res.status == 'ok', res.reason);
    })
    .catch(error => {
      // console.log('error', error)
      error = getError(error);
      // console.log('error', error)
      callback(false, error);
    })
}

export function addGift(data, photoUri, callback) {
  // console.log('add task')
  let jsondata = JSON.stringify(data);

  let form = new FormData();
  form.append('data', jsondata);
  if (photoUri){
    form.append('photo', {uri: photoUri, name: 'photo.jpg', type: 'image/jpg'});
  }

  fetch(hostAddr + '/api/add_gift', {
    method: 'POST',
    credentials: 'same-origin',
    headers: {'Content-Type': 'multipart/form-data'},
    body: form
  })
    .then(resp => {
      console.log('add task resp', resp);
      return resp.json();
    })
    .then(res => {
      console.log('resp json', res)
      callback(res.status == 'ok', res.reason);
    })
    .catch(error => {
      console.log('error', error)
      error = getError(error);
      console.log('error', error)
      callback(false, error);
    })
}

export function editGift(data, photoUri, callback) {
  // console.log('edit gift')
  let jsondata = JSON.stringify(data);

  let form = new FormData();
  form.append('data', jsondata);
  if (photoUri){
    form.append('photo', {uri: photoUri, name: 'photo.jpg', type: 'image/jpg'});
  }

  fetch(hostAddr + '/api/edit_gift', {
    method: 'POST',
    credentials: 'same-origin',
    headers: {'Content-Type': 'multipart/form-data'},
    body: form
  })
    .then(resp => {
      // console.log('edit gift resp', resp);
      return resp.json();
    })
    .then(res => {
      // console.log('resp json', res)
      callback(res.status == 'ok', res.reason);
    })
    .catch(error => {
      // console.log('error', error)
      error = getError(error);
      // console.log('error', error)
      callback(false, error);
    })
}

export function removeGift(gift_id, callback) {
  // console.log('remove gift')

  let data = {gift_id}

    fetch(hostAddr + '/api/remove_gift', {
      method: 'POST',
      credentials: 'same-origin',
      body: JSON.stringify(data),
    })
    .then(resp => {
      // console.log('remove gift resp', resp);
      return resp.json();
    })
    .then(res => {
      // console.log('resp json', res)
      callback(res.status == 'ok', res.reason);
    })
    .catch(error => {
      // console.log('error', error)
      error = getError(error);
      // console.log('error', error)
      callback(false, error);
    })
}

export function buyGift(gift_id, callback) {
  // console.log('buy gift')

  let data = {gift_id: gift_id}

  fetch(hostAddr + '/api/buy_gift', {
      method: 'POST',
      credentials: 'same-origin',
      body: JSON.stringify(data),
    })
    .then(resp => {
      // console.log('buy gift res', resp);
      return resp.json();
    })
    .then(res => {
      // console.log('resp json', res)
      callback(res.data, res.reason);
    })
    .catch(error => {
      // console.log('error', error)
      error = getError(error);
      // console.log('error', error)
      callback(undefined, error);
    })
}

export function confirmGift(gift_id, callback) {
  // console.log('confirm gift')

  let data = {gift_id: gift_id}

  fetch(hostAddr + '/api/confirm_gift', {
      method: 'POST',
      credentials: 'same-origin',
      body: JSON.stringify(data),
    })
    .then(resp => {
      // console.log('confirm gift res', resp);
      return resp.json();
    })
    .then(res => {
      // console.log('resp json', res)
      callback(res.data, res.reason);
    })
    .catch(error => {
      // console.log('error', error)
      error = getError(error);
      // console.log('error', error)
      callback(undefined, error);
    })
}

export function awardChild(child_id, amount, callback) {
  // console.log('award child')

  let data = {child_id, amount}

  fetch(hostAddr + '/api/award', {
      method: 'POST',
      credentials: 'same-origin',
      body: JSON.stringify(data),
    })
    .then(resp => {
      // console.log('award child res', resp);
      return resp.json();
    })
    .then(res => {
      // console.log('resp json', res)
      callback(res.data, res.reason);
    })
    .catch(error => {
      // console.log('error', error)
      error = getError(error);
      // console.log('error', error)
      callback(undefined, error);
    })
}

export function getChat(user_id, callback) {
  // console.log('get chat')

  let data = {'user_id': user_id}

  fetch(hostAddr + '/api/chat', {
      method: 'POST',
      credentials: 'same-origin',
      body: JSON.stringify(data),
    })
    .then(resp => {
      // console.log('get chat res', resp);
      return resp.json();
    })
    .then(res => {
      // console.log('resp json', res)
      callback(res.data, res.reason);
    })
    .catch(error => {
      // console.log('error', error)
      error = getError(error);
      // console.log('error', error)
      callback(undefined, error);
    })
}

export function sendMessage(user_id, text, callback) {
  // console.log('send message')

  let data = {'user_id': user_id, text: text}

  fetch(hostAddr + '/api/send_message', {
      method: 'POST',
      credentials: 'same-origin',
      body: JSON.stringify(data),
    })
    .then(resp => {
      // console.log('send mess res', resp);
      return resp.json();
    })
    .then(res => {
      // console.log('resp json', res)
      callback(res.data, res.reason);
    })
    .catch(error => {
      // console.log('error', error)
      error = getError(error);
      // console.log('error', error)
      callback(undefined, error);
    })
}