import React, {useState,  useEffect} from 'react';
import { Form, Button, Row, Col } from 'react-bootstrap';
import {checkAuth, login as loginUser} from '../backend';
import {Redirect} from 'react-router-dom';
function Login() {
  const [authCheckt, setauthCheckt] = useState(false);
  const [isLogin, setisLogin] = useState(false);
  useEffect(() => {
    checkAuth((res, error)=>{
      if (!error && res){
        setisLogin(true);
        setauthCheckt(true);
      }
    })
  }, [0])
  const [login, setlogin] = useState('')
  const [password, setpassword] = useState('')
  console.log(login)
  const onLogin = ()=>{
    loginUser(login, password, (res, error)=>{
      if (!error && res){
        console.log("login res", res, error)
        setauthCheckt(true);
        setisLogin(true);
      }
    })
  }
  if (authCheckt && isLogin){
    return <Redirect to={{pathname:'/'}}></Redirect>
  }
  return (
    <Row>
      <Col md={{span:4, offset:4}}>
        <Form>
          <Form.Group controlId="formBasicLogin">
            <Form.Control type="text" value={login} onChange={(e)=>setlogin(e.target.value)} placeholder="Login" />
          </Form.Group>

          <Form.Group controlId="formBasicPassword">
            <Form.Control type="password" value={password} onChange={(e)=>setpassword(e.target.value)} placeholder="Password" />
          </Form.Group>
          <Form.Group controlId="formBasicChecbox">
            <Form.Check type="checkbox" label="Check me out" />
          </Form.Group>
          <Button variant="primary" onClick={onLogin}>
            Submit
          </Button>
        </Form>
      </Col>
    </Row>
  );
}

export default Login;