import React from 'react';
import './App.css';
import Login from './intro/Login';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Main from './main/Main';

function App() {
  return (
    <Router>
      <Switch>
        <Route path="/login" exact component={Login} />
        <Route path="/" component={Main} />
      </Switch>
    </Router>
  );
}

export default App;
