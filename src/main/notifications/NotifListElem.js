import React, {useState} from 'react';
import {imageUrl} from '../../backend';
import './NotifListElem.css' 

class NotifListElem extends React.Component{
  constructor(props){
    super(props);
  }
  
  render(){
      const notifMessages={
          1:'добавил задание',
          2:'изменил задание',
          3:'напоминает о задании',
          4:'удалил задание',
          5:'подтвердил задание',
          6:'добавил подарок',
          7:'изменил подарок',
          8:'удалил подарок',
          9:'подтвердил подарок',
          10:'выполнил задание',
          11:'купил подарок',
      }
      return (
        <div className='notif-container'>
          <div className='notif-img'>
            <img src={imageUrl(this.props.notif.from_user.image)}></img>
          </div>
          <div className='notif-text-cont'>
            <div className='notif-text'>
              {this.props.notif.from_user.name} {notifMessages[this.props.notif.type]}
            </div>
          </div>
        </div>
      );
    }
  }
  
  export default NotifListElem;