import React from 'react';
import { Row, Col} from 'react-bootstrap';
import {checkAuth, getNotifList} from '../../backend';
import NotifListElem from './NotifListElem';


class Notif extends React.Component {
  constructor(props){
    super(props)
    this.state={
      notifs:[],
      role:undefined
    }
  }

  refreshData(){
    if (!this.state.role){
      checkAuth((res,error)=>{
        if (!error && res){
          this.setState({role:res.role}, ()=>{
            this.refreshData()
          })
        }
        else{
          this.refreshData()
        }
      })
    }
    else{
      getNotifList((res, error)=>{
        if (!error && res){
          this.setState({notifs:res})
        }
      })
    }
  }
  componentDidMount(){
    setInterval(()=>{
      this.refreshData()
    }, 1000)
  }  
  
  render(){
     return (
      <div>
      {this.state.notifs.map((notif)=>(
        <div>
          <NotifListElem notif={notif} ></NotifListElem>
        </div>
      ))}
      </div>
  );
}
}

export default Notif;
