import React, {useState,  useEffect} from 'react';
import SideMenu from './side_menu/SideMenu';
import { Row, Col, ListGroup, ListGroupItem } from 'react-bootstrap';
import { BrowserRouter as Router, Route } from "react-router-dom";
import Home from './home/Home';
import Family from './family/Family';
import Notif from './notifications/Notif'
import Settings from './settings/Settings';
import {Redirect} from 'react-router-dom';
import {checkAuth} from '../backend';

function Main() {
  const [authCheckt, setauthCheckt] = useState(false);
  const [isLogin, setisLogin] = useState(false);
  useEffect(() => {
    checkAuth((res, error)=>{
      if (!error && res){
        setisLogin(true);
        setauthCheckt(true);
      }
      else{
        setauthCheckt(true);
        setisLogin(false);
      }
    })
  }, [0])
  if (authCheckt && !isLogin){
    return <Redirect to={{pathname:'/login'}}></Redirect>
  }

  return (
    <Router>
      <div className='content'>
        <div className='contant-container'>
          <Row>
            <Col sm={2}>
              <SideMenu/>
            </Col>
            <Col sm={10}>
              <Route path="/" exact component={Home} />
              <Route path="/family/" component={Family} />
              <Route path="/settings/" component={Settings} />
              <Route path="/notifications/" component={Notif} />
            </Col>
          </Row>
        </div>
      </div>
    </Router>
  );
}

export default Main;