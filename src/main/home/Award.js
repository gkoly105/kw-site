import React from 'react';
import { Modal, Button, Form } from 'react-bootstrap';
import _ from 'lodash';
import {awardChild} from '../../backend';

class Award extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      award:'',
      errors:{}
    }
  }
  onSubmit(){
    let award = this.state.award
    let errors = {}
    award = Number(award)
    if (isNaN(award) || award <= 0){
      errors.title = 'Неверный формат';
    }
    this.setState({errors})
    if (JSON.stringify(errors) === '{}'){
      awardChild(this.props.user.id,award,(res,error)=>{

      })
      this.props.onHide();
    }

  }
  render() {
    return (
      <Modal
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
        {...this.props}
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Поощеряем
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
        <Form>
            <Form.Group controlId="Award">
              <Form.Label>Сколько</Form.Label>
              <Form.Control type="text" value={this.state.award} onChange={(e)=>this.setState({award:e.target.value})}/>
              <Form.Text className="text-muted">
                We'll never share your email with anyone else.
              </Form.Text>
            </Form.Group>
            <Button variant="primary" onClick={()=>this.onSubmit()}>
              Submit
            </Button>
        </Form>;
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={this.props.onHide}>Close</Button>
        </Modal.Footer>
      </Modal>
    );
  }
}
  
  export default Award;