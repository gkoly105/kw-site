import React from 'react';
import { Modal, Button, Form } from 'react-bootstrap';
import _ from 'lodash';
import {addTask, editTask, penaltyChild} from '../../backend';

class PenaltyTask extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      penalty:'',
      errors:{}
    }
  }
  onSubmit(){
    let penalty = this.state.penalty
    let errors = {}
    let id = ''
    penalty = Number(penalty)
    if (isNaN(penalty) || penalty <= 0){
      errors.title = 'Неверный формат';
    }
    this.setState({errors})
    if (this.props.task){
      id = this.props.task.id
    }
    else{
      id = undefined;
    }
    if (JSON.stringify(errors) === '{}'){
      penaltyChild(this.props.user.id, penalty, id,(res, error)=>{

      })
      this.props.onHide();
    }

  }
  render() {
    return (
      <Modal
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
        {...this.props}
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Штрафуем
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
        <Form>
            <Form.Group controlId="Penalty">
              <Form.Label>Сколько</Form.Label>
              <Form.Control type="text" value={this.state.penalty} onChange={(e)=>this.setState({penalty:e.target.value})}/>
              <Form.Text className="text-muted">
                We'll never share your email with anyone else.
              </Form.Text>
            </Form.Group>
            <Button variant="primary" onClick={()=>this.onSubmit()}>
              Submit
            </Button>
        </Form>;
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={this.props.onHide}>Close</Button>
        </Modal.Footer>
      </Modal>
    );
  }
}
  
  export default PenaltyTask;