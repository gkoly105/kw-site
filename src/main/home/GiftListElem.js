import React from 'react';
import { Collapse, Button, Form, Modal } from 'react-bootstrap';
import './TaskListElem.css'
import {doTask, confirmTask, remindTask, penaltyChild, confirmGift} from '../../backend';
import AddTask from './AddTask';
import PenaltyTask from './PenaltyTask';
import AddGift from './AddGift';

class GiftListElem extends React.Component {
    constructor(props, context) {
      super(props, context);
  
      this.state = {
        open: false,
        do: false,
        confirm:false,
        showAddGift:false,
        showPenalty:false,
      };
    }
    
    giftBuy(){
      confirmGift(this.props.gift.id,(res, error)=>{

      })
      //console.log(this.props.task.finish_time)
    }

    giftConfirm(){
      if (this.props.gift.buying_time){
          confirmGift(this.props.gift.id,(res, error)=>{

        })
      }
      //console.log(this.props.task.finish_time)
    }

    render() {
      let modalClose = () => this.setState({ showAddGift: false });
      const { open } = this.state;
      if (this.props.role === 'parent'){
        return(
            <div className={'task '+(this.props.gift.buying_time ? 'do': '')}>
              <div
                onClick={() => this.setState({ open: !open })}
                aria-controls="example-collapse-text"
                aria-expanded={open}
              >
              {this.props.gift.title}
              </div>
              <Collapse in={this.state.open}>
                <div id="example-collapse-text">
                  <div className='task-container'>
                    <div className='task-buttons'>
                      <img onClick={()=>{this.giftConfirm()}} src = {require('../../res/icons/Confirm.png')}></img>
                    </div>
                    <div className='task-buttons'>
                      <img onClick={()=>this.setState({showAddGift:true})} src = {require('../../res/icons/Settings.png')}></img>
                    </div>
                  </div>
                </div>
              </Collapse>
              <AddGift
                show={this.state.showAddGift}
                onHide={modalClose}
                user = {this.props.user}
                gift = {this.props.gift}
                mode = 'edit'
              />
            </div>
        )
      }
      return (
        <div className={'task '+(this.props.gift.buying ? 'do': '')}>
          <div
            onClick={() => this.setState({ open: !open })}
            aria-controls="example-collapse-text"
            aria-expanded={open}
          >
          {this.props.gift.title}
          </div>
          <Collapse in={this.state.open}>
            <div id="example-collapse-text">
              <div className='task-container'>
                <div className='task-buttons'>
                  <img onClick={()=>{this.giftBuy()}} src = {require('../../res/icons/Confirm.png')}></img>
                </div>
              </div>
            </div>
          </Collapse>
        </div>
      );
    }
  }

  export default GiftListElem;