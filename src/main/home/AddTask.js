import React from 'react';
import { Modal, Button, Form } from 'react-bootstrap';
import _ from 'lodash';
import {addTask, editTask, removeTask} from '../../backend';

class AddTask extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      title:'',
      desk:'',
      reward:'',
      require_image:false,
      errors:{}
    }
    if (this.props.mode === 'edit'){
      this.state.title = this.props.task.title
      this.state.desc = this.props.task.desc
      this.state.reward = this.props.task.reward
      this.state.require_image = this.props.task.require_image
    }
  }
  onSubmit(){
    let data = _.pick(this.state, 'title', 'desc', 'reward', 'require_image')
    let errors = {}
    if (!data.title) errors.title = 'Необходимо заполнить';
    if (!data.reward) errors.reward = 'Необходимо заполнить';
    else {
      data.reward = Number(data.reward)
      if (isNaN(data.reward) || data.reward <= 0){
        errors.title = 'Неверный формат';
      }
    }
    this.setState({errors})

    if (JSON.stringify(errors) === '{}'){
      if (this.props.mode === 'edit'){
        data.task_id = this.props.task.id
        editTask(data, (res, error)=>{

        })
      }
      else{
        data.child_id = this.props.user.id
        addTask(data, (res, error)=>{

        })
      }
      this.props.onHide();
    }
  }

  TaskDelete(){
    removeTask(this.props.task.id, (res, error)=>{

    })
  }

  render() {
    return (
      <Modal
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
        {...this.props}
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Добавления Задания
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
        <Form>
            <Form.Group controlId="Title">
              <Form.Label>Название</Form.Label>
              <Form.Control type="text" value={this.state.title} onChange={(e)=>this.setState({title:e.target.value})}/>
              <Form.Text className="text-muted">
                We'll never share your email with anyone else.
              </Form.Text>
            </Form.Group>
          
            <Form.Group controlId="Discription">
              <Form.Label>Описание</Form.Label>
              <Form.Control as='textarea' rows='3' value={this.state.desc} onChange={(e)=>this.setState({desc:e.target.value})}/>
            </Form.Group>
            <Form.Group controlId="formBasicChecbox">
              <Form.Check type="checkbox"  value={this.state.require_image} onChange={(e)=>this.setState({require_image:e.target.value})} label="Подтверждение фотографией" />
            </Form.Group>
            <Form.Group controlId="Price">
              <Form.Label>Цена</Form.Label>
              <Form.Control type="text" value={this.state.reward} onChange={(e)=>this.setState({reward:e.target.value})}/>
              <Form.Text className="text-muted">
                We'll never share your email with anyone else.
              </Form.Text>
            </Form.Group>
            <Button variant="primary" onClick={()=>this.onSubmit()}>
              {this.props.mode === 'edit' ? 'Изменить' : 'Добавить'}
            </Button>
            {this.props.mode === 'edit' &&
              <Button variant="primary" onClick={()=>this.TaskDelete()}>
                Удалить
              </Button>
            }
        </Form>;
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={this.props.onHide}>Close</Button>
        </Modal.Footer>
      </Modal>
    );
  }
}
  
  export default AddTask;