import React from 'react';
import TaskListElem from './TaskListElem';
import './TaskAndGifts.css'
import AddTask from './AddTask';
import AddGift from './AddGift';
import GiftListElem from './GiftListElem';

class TasksAndGifts extends React.Component{
    constructor(props){
        super(props)
        this.state={
          tabIndex:0,
          showAddTask:false,
          showAddGift:false,
        }
      }

    getItemStyle(index){
      if (index === this.state.tabIndex){
        return{
          fontSize:'50px',
        }
      }
      else{
        return{
          fontSize:'16px',
        }
      }
    }
    render(){
    let modalClose = () => this.setState({ showAddTask: false });
    let modalCloseGift = () => this.setState({ showAddGift: false });
        return (
          <div>
            <div className='tasks-gifts'>
                <div onClick={()=>{this.setState({tabIndex:0})}} className={'tasks '+(this.state.tabIndex === 0 ? 'active': '')}>
                  <div className='tasks-text'>
                    Задания
                  </div>
                  { this.props.role == 'parent' &&
                    <div onClick={()=>this.setState({showAddTask:true})} className='tasks-img'>
                      <img src = {require('../../res/icons/Add.png')}></img>
                    </div>
                  }
                </div>
                <div onClick={()=>{this.setState({tabIndex:1})}} className = {'gifts '+(this.state.tabIndex === 1 ? 'active': '')}>
                  <div className='gifts-text'>
                    Подарки
                  </div>
                  { this.props.role == 'parent' &&
                    <div onClick={()=>this.setState({showAddGift:true})} className='gift-img'>
                      <img src = {require('../../res/icons/Add.png')}></img>
                    </div>
                  }
                </div>
            </div>
            <div>
                { this.props.user &&
                  <div>
                    { this.state.tabIndex === 0 &&
                      this.props.user.taskList.map((item)=>(
                        <div>
                          <TaskListElem user={this.props.user} role={this.props.role} task={item}></TaskListElem>
                        </div>
                    ))}
                    { this.state.tabIndex === 1 &&
                      this.props.user.giftList.map((item)=>(
                        <div>
                          <GiftListElem user={this.props.user} role={this.props.role} gift={item}/>
                        </div>
                    ))}
                  </div>
                }
            </div>
            <AddTask 
              show={this.state.showAddTask}
              onHide={modalClose}
              user = {this.props.user}
            />
            <AddGift
              show={this.state.showAddGift}
              onHide={modalCloseGift}
              user = {this.props.user}
            />
          </div>            
        );
      }
    }
    
    export default TasksAndGifts;
