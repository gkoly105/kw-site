import React from 'react';
import { Collapse, Button, Form, Modal } from 'react-bootstrap';
import './TaskListElem.css'
import {doTask, confirmTask, remindTask, penaltyChild} from '../../backend';
import AddTask from './AddTask';
import PenaltyTask from './PenaltyTask';

class TaskListElem extends React.Component {
    constructor(props, context) {
      super(props, context);
  
      this.state = {
        open: false,
        do: false,
        confirm:false,
        showAddTask:false,
        showPenalty:false,
      };
    }
    
    taskDo(){
      doTask(this.props.task.id,(res, error)=>{

      })
      //console.log(this.props.task.finish_time)
    }

    taskConfirm(){
      if (this.props.task.finish_time){
          confirmTask(this.props.task.id,(res, error)=>{

        })
      }
      //console.log(this.props.task.finish_time)
    }

    taskRemind(){
      remindTask(this.props.task.id,(res, error)=>{

      })
      //console.log(this.props.task.finish_time)
    }


    

    render() {
      let modalClose = () => this.setState({ showAddTask: false });
      let modalClosePenalty = () => this.setState({ showPenalty: false });
      const { open } = this.state;
      if (this.props.role == 'parent'){
        return(
            <div className={'task '+(this.props.task.finish_time ? 'do': '')}>
              <div
                onClick={() => this.setState({ open: !open })}
                aria-controls="example-collapse-text"
                aria-expanded={open}
              >
              {this.props.task.title}
              </div>
              <Collapse in={this.state.open}>
                <div id="example-collapse-text">
                  <div className='task-container'>
                    <div className='task-buttons'>
                      <img onClick={()=>{this.taskConfirm()}} src = {require('../../res/icons/Confirm.png')}></img>
                    </div>
                    <div className='task-buttons'>
                      <img onClick={()=>{this.taskRemind()}} src = {require('../../res/icons/Remind.png')}></img>
                    </div>
                    <div className='task-buttons'>
                      <img onClick={()=>this.setState({showPenalty:true})} src = {require('../../res/icons/Dislike_grey.png')}></img>
                    </div>
                    <div className='task-buttons'>
                      <img onClick={()=>this.setState({showAddTask:true})} src = {require('../../res/icons/Settings.png')}></img>
                    </div>
                  </div>
                </div>
              </Collapse>
              <AddTask 
                show={this.state.showAddTask}
                onHide={modalClose}
                user = {this.props.user}
                task = {this.props.task}
                mode = 'edit'
              />
              <PenaltyTask
                show={this.state.showPenalty}
                onHide={modalClosePenalty}
                user = {this.props.user}
                task = {this.props.task}
              />
            </div>
        )
      }
      return (
        <div className={'task '+(this.props.task.finish_time ? 'do': '')}>
          <div
            onClick={() => this.setState({ open: !open })}
            aria-controls="example-collapse-text"
            aria-expanded={open}
          >
          {this.props.task.title}
          </div>
          <Collapse in={this.state.open}>
            <div id="example-collapse-text">
              <div className='task-container'>
                <div className='task-buttons'>
                  <img onClick={()=>{this.taskDo()}} src = {require('../../res/icons/Confirm.png')}></img>
                </div>
              </div>
            </div>
          </Collapse>
        </div>
      );
    }
  }

  export default TaskListElem;