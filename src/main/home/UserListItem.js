import React, {useState} from 'react';
import {imageUrl} from '../../backend';
import './UserListItem.css' 
import PenaltyTask from './PenaltyTask';
import Award from './Award';
class UserListItem extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      showPenalty:false,
      showAward:false,
    };
  }

  render(){
    let modalClosePenalty = () => this.setState({ showPenalty: false });
    let modalCloseAward = () => this.setState({ showAward: false });
      return (
        <div className = {'profile-container '+(this.props.active ? 'active': '')}>
          <div className = 'profile-img'>
            <img src={imageUrl(this.props.user.image)}></img>
          </div>
          { this.props.role === 'parent' &&
          <div className='penalty-award'>
            <div className='award' onClick={()=>this.setState({showAward:true})}>
              <img src={require('../../res/icons/Like.png')}></img>
            </div>
            <div className='penalty' onClick={()=>this.setState({showPenalty:true})}>
              <img src={require('../../res/icons/Dislike.png')}></img>
            </div>
          </div>
        }
          <div className = 'profile-data'>
            <div className = 'profile-data-container'>
              <div>
                <span>{this.props.user.name}</span>
              </div>
            </div>
            <div className = 'profile-data-container'>
              <div>
                <span>{this.props.user.points}</span>
              </div>
              <div>
                <img src = {require('../../res/icons/Money.png')}></img>
              </div>
            </div>
            <div className = 'profile-data-container'>
              <div>
                <span>{this.props.user.tasks}</span>
              </div>
              <div>
                <img src = {require('../../res/icons/Task.png')}></img>
              </div>
            </div>
            <div className = 'profile-data-container'>
              <div>
                <span>{this.props.user.gifts}</span>
              </div>
              <div>
                <img src = {require('../../res/icons/Gift.png')}></img>
              </div>
            </div>
          </div>
          <PenaltyTask
            show={this.state.showPenalty}
            onHide={modalClosePenalty}
            user = {this.props.user}
            task = {undefined}
          />
          <Award
            show={this.state.showAward}
            onHide={modalCloseAward}
            user = {this.props.user}
          />
        </div>
      );
    }
  }
  
  export default UserListItem;