import React from 'react';
import { Modal, Button, Form } from 'react-bootstrap';
import {sendMessage} from '../../../backend';
import Messages from './Messages';
import './Chat.css';

class Chat extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      text:'',
    }
  }
  Send(){
    let text = this.state.text;
    if (text){
      sendMessage(this.props.user.user_id, text, (res, error)=>{
        this.setState({text:''})
      })
    }
  }
  render() {
    return (
        <div className='chat'>
          <h1>Чат</h1>
          <div className='messages'>
          { this.props.user &&
            <Messages user = {this.props.user}></Messages>
          }
          </div>
          <div className='chat-controls'>
            <div className='entry'>
              <input value={this.state.text} onChange={(e)=>this.setState({text:e.target.value})}></input>
            </div>
            <div className='send' onClick={()=>this.Send()}>
              <img src={require('../../../res/icons/Send_invitation.png')}></img>
            </div>
          </div>
      </div>
    );
  }
}
  
  export default Chat;