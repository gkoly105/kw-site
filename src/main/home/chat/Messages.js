import React from 'react';
import { Modal, Button, Form } from 'react-bootstrap';
import _ from 'lodash';
import {getChat} from '../../../backend';
import './Messages.css';

class Messages extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      award:'',
      errors:{},
      messages:[],
    }
  }
  refreshData(){
    getChat(this.props.user.user_id,(res, error)=>{
      if (!error && res){
        this.setState({messages:res['messages']})
        this.messagesEnd.scrollIntoView({ behavior: "smooth" });  
      }
    })
    
  }
  componentDidMount(){
    setInterval(()=>{
      this.refreshData()
    }, 1000)
  }
  render() {
    return (
      <div className='messages'>
      { this.state.messages.map((message)=>(
        <div className='message-container'>
          <div className={'message '+(message.from_id === this.props.user.user_id ? '': 'right')}>
            <span>{message.text}</span>
          </div>
        </div>
      ))}
      <div style={{ float:"left", clear: "both" }}
             ref={(el) => { this.messagesEnd = el; }}>
        </div>
      </div>
    );
  }
}
  
  export default Messages;