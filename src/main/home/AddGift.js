import React from 'react';
import { Modal, Button, Form } from 'react-bootstrap';
import _ from 'lodash';
import {addTask, addGift} from '../../backend';

class AddGift extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      title:'',
      desc:'',
      cost:'',
      errors:{}
    }
  }
  onSubmit(){
    let data = _.pick(this.state, 'title', 'desc', 'cost')
    data.child_id = this.props.user.id
    let errors = {}
    if (!data.title) errors.title = 'Необходимо заполнить';
    if (!data.cost) errors.reward = 'Необходимо заполнить';
    else {
      data.cost = Number(data.cost)
      if (isNaN(data.cost) || data.cost <= 0){
        errors.cost = 'Неверный формат';
      }
    }
    this.setState({errors})

    if (JSON.stringify(errors) === '{}'){
      addGift(data, undefined, (res, error)=>{

      })
      this.props.onHide();
    }

  }
  render() {
    return (
      <Modal
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
        {...this.props}
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Добавления Подарка
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
        <Form>
            <Form.Group controlId="Title">
              <Form.Label>Название</Form.Label>
              <Form.Control type="text" value={this.state.title} onChange={(e)=>this.setState({title:e.target.value})}/>
              <Form.Text className="text-muted">
                We'll never share your email with anyone else.
              </Form.Text>
            </Form.Group>
          
            <Form.Group controlId="Discription">
              <Form.Label>Описание</Form.Label>
              <Form.Control as='textarea' rows='3' value={this.state.desc} onChange={(e)=>this.setState({desc:e.target.value})}/>
            </Form.Group>
            <Form.Group controlId="Price">
              <Form.Label>Цена</Form.Label>
              <Form.Control type="text" value={this.state.cost} onChange={(e)=>this.setState({cost:e.target.value})}/>
              <Form.Text className="text-muted">
                We'll never share your email with anyone else.
              </Form.Text>
            </Form.Group>
            <Button variant="primary" onClick={()=>this.onSubmit()}>
              {this.props.mode === 'edit' ? 'Изменить' : 'Добавить'}
            </Button>
        </Form>;
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={this.props.onHide}>Close</Button>
        </Modal.Footer>
      </Modal>
    );
  }
}
  
  export default AddGift;