import React from 'react';
import { Row, Col} from 'react-bootstrap';
import {getChildList, checkAuth, getParentList} from '../../backend';
import TasksAndGifts from './TasksAndGifts';
import UserListItem from './UserListItem';
import './Home.css';
import Chat from './chat/Chat';

class Home extends React.Component {
  constructor(props){
    super(props)
    this.state={
      users:[],
      userIndex:0,
      role:undefined,
      admin_id:undefined,
    }
  }

  refreshData(){
    if (!this.state.role){
      checkAuth((res,error)=>{
        if (!error && res){
          this.setState({role:res.role}, ()=>{
            this.refreshData()
          })
        }
        else{
          this.refreshData()
        }
      })
    }
    else if (this.state.role === 'parent'){
      getChildList((res, error)=>{
        if (!error && res){
          this.setState({users:res})
        }
      })
    }
    else if (this.state.role === 'child'){
      getParentList((res, error)=>{
        if (!error && res){
          this.setState({users:res})
        }
      })
    }
  }
  componentDidMount(){
    setInterval(()=>{
      this.refreshData()
    }, 1000)
  }  
  
  getItemStyle(index){
    if (index === this.state.userIndex){
      return{
        fontSize:'50px',
      }
    }
    else{
      return{

      }
    }
  }

  getUserData(){
    if (this.state.userIndex !== undefined && this.state.users.length){
      return this.state.users[this.state.userIndex];
    }
    else{
      return null
    }
  }
  render(){
     return (
      <div>
        <Row>
          <Col sm={9} className='chat-task-gifts'>
            <Row className='container'>
              <Col sm={5} className='container-tasks-gifts'>
                <TasksAndGifts role={this.state.role} user={this.getUserData()}/>
              </Col>
              <Col sm={{span:6, offset:1 }} className = 'container-chat'>
                <div className='chat'>
                  <Chat user={this.getUserData()}></Chat>
                </div>
              </Col>
            </Row>
          </Col>
          <Col sm={3}>
            {this.state.users.map((user, index)=>(
              <div onClick={()=>{this.setState({userIndex:index})}}>
                <UserListItem role={this.state.role} active = {index === this.state.userIndex} user = {user}></UserListItem>
              </div>
            ))}
          </Col>
        </Row>
      </div>
  );
}
}

export default Home;
