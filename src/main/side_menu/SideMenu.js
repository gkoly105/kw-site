import React from 'react';
import { BrowserRouter as Router, Route, Link, Redirect } from "react-router-dom";
import { ListGroup,} from 'react-bootstrap';
import './SideMenu.css'
import {getProfile, imageUrl, logout} from '../../backend';


class SideMenu extends React.Component{
  constructor(props){
    super(props);
    this.state={
      profile:null,
      out:false,
    }
  }
  getImage(){
    return this.state.profile && imageUrl(this.state.profile.user.image);
  }

  getName(){
    return this.state.profile && this.state.profile.user.name
  }

  refreshData(){
    getProfile((res, error)=>{
      if (!error && res){
        this.setState({profile:res})
      }
    })
  }

  onLogout(){
    console.log('exit')
    logout((res, error)=>{
      if (!error && res){
        this.setState({out:true})
      }
    })
  }

  componentDidMount(){
    setInterval(()=>{
      this.refreshData()
    }, 1000)
  }  
  
  render(){
    if (this.state.out === true){
      return <Redirect to={{pathname:'/login'}}></Redirect>
    }
    return (
      <div className='side-menu-container'>
        <div className='profile'>
          <img src={this.getImage()}></img>
          <span>{this.getName()}</span>
        </div>
        <ListGroup defaultActiveKey="#link1">
          <Link to="/">
            <ListGroup.Item action href="#link1">
              Главная
            </ListGroup.Item>
          </Link>
          <Link to="/family/">
            <ListGroup.Item action href="#link2">
              Семья
            </ListGroup.Item>
          </Link>
          <Link to="/settings/">
            <ListGroup.Item action href="#link3">
              Настройки
            </ListGroup.Item>
          </Link>
          <Link to="/notifications/">
            <ListGroup.Item action href="#link5">
              Уведомления
            </ListGroup.Item>
          </Link>
          <ListGroup.Item action href="#link4" onClick={this.onLogout.bind(this)}>
            Выход
          </ListGroup.Item>
        </ListGroup>
      </div>            
      );
    }
  }
  
  export default SideMenu;
